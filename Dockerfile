FROM python:3.10-slim

ENV APP_ROOT=/app

RUN apt update && apt install -y \
    gcc \
    make \
    libpq-dev

COPY app/poetry.lock app/pyproject.toml $APP_ROOT/
COPY app $APP_ROOT/app

WORKDIR $APP_ROOT

RUN pip install -U pip && \
    pip install poetry

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

WORKDIR $APP_ROOT/app