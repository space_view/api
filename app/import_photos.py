"""
Импорт новых фото из парсера через чтение из очередей
"""
import asyncio
import json

import aio_pika
from sqlalchemy.exc import IntegrityError

from config import settings
from crud.raw_photo_crud import RawPhotoCRUD
from models.db import async_session_factory


async def raw_photo_import(message: aio_pika.abc.AbstractIncomingMessage):
    async with async_session_factory() as session:
        raw_photos_crud = RawPhotoCRUD(session=session)
        async with message.process():
            data = json.loads(message.body)
            try:
                print(data)
                await raw_photos_crud.create(
                    data={
                        'full_path': data.get('full_path'),
                        'category': data.get('category'),
                        'img_urls': data.get('img_urls'),
                        'title': data.get('title', '').strip(),
                        'type': data.get('type'),
                    }
                )
            except IntegrityError:
                print('Такое фото уже есть в базе')
            # await asyncio.sleep(1)


async def consume():
    connection = await aio_pika.connect_robust(
        host=settings.rabbitmq_host,
        port=settings.rabbitmq_port,
        virtualhost=settings.rabbitmq_vhost,
        login=settings.rabbitmq_login,
        password=settings.rabbitmq_password,
    )
    channel = await connection.channel()
    await channel.set_qos(prefetch_count=10)
    queue = await channel.declare_queue(settings.rabbitmq_queue, durable=True)
    await queue.consume(raw_photo_import)

    try:
        await asyncio.Future()
    finally:
        await connection.close()


if __name__ == '__main__':
    asyncio.run(consume())
