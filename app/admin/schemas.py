from pydantic import BaseModel

from models.raw_photo import RawPhotoStatus


class RawPhotoScheme(BaseModel):
    id: int
    title: str
    full_path: str
    img_urls: list[str]
    type: str | None
    category: str | None

    class Config:
        orm_mode = True


class RawPhotoChangeStatus(BaseModel):
    status: RawPhotoStatus
