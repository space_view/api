from fastapi import APIRouter, Depends

from admin import raw_photos_api
from admin.dependencies import check_admin_user

admin_router = APIRouter(prefix='/admin', dependencies=[Depends(check_admin_user)])

admin_router.include_router(raw_photos_api.router)
