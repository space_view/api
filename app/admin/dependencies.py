from fastapi import Depends, Header, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from config import settings
from crud.raw_photo_crud import RawPhotoCRUD
from models.db import get_session


def check_admin_user(x_admin_token: str = Header()) -> None:
    if settings.admin_access_key and x_admin_token != settings.admin_access_key:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
        )


def get_raw_photo_crud(db_session: AsyncSession = Depends(get_session)) -> RawPhotoCRUD:
    return RawPhotoCRUD(session=db_session)
