from fastapi import APIRouter, Depends, HTTPException
from starlette.status import HTTP_200_OK

from admin.dependencies import get_raw_photo_crud
from admin.schemas import RawPhotoScheme
from crud.crud_base import CRUDException
from crud.raw_photo_crud import RawPhotoCRUD
from models.raw_photo import RawPhotoStatus

router = APIRouter(prefix='/raw_photo')


@router.get('/', response_model=RawPhotoScheme)
async def get_photo_for_review(crud: RawPhotoCRUD = Depends(get_raw_photo_crud)):
    raw_photos = await crud.get_photos_by_status(status=RawPhotoStatus.NEW, limit=1)
    if raw_photos:
        return RawPhotoScheme.from_orm(raw_photos[0])
    else:
        raise HTTPException(status_code=404, detail='Фото не найдено')


@router.patch('/{photo_id}/', status_code=HTTP_200_OK)
def approve_photo(photo_id: int, status: RawPhotoStatus, crud: RawPhotoCRUD = Depends(get_raw_photo_crud)):
    try:
        return crud.change_status_photo(photo_id=photo_id, new_status=status)
    except CRUDException:
        return
