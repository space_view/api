from typing import Annotated, AsyncGenerator

from sqlalchemy import ARRAY, String
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine
from sqlalchemy.orm import DeclarativeBase, Mapped, MappedAsDataclass, mapped_column

from config import settings

engine = create_async_engine(settings.postgres_url, echo=True)
async_session_factory = async_sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


async def get_session() -> AsyncGenerator:
    async with async_session_factory() as session:
        yield session


class Base(MappedAsDataclass, DeclarativeBase):
    id: Mapped[int] = mapped_column(init=False, primary_key=True)


str_100 = Annotated[str, mapped_column(String(100))]
str_255 = Annotated[str, mapped_column(String(255))]
str_300 = Annotated[str, mapped_column(String(300))]
str_400 = Annotated[str, mapped_column(String(400))]
list_str_400 = Annotated[list[str], mapped_column(ARRAY(String(length=400)))]
