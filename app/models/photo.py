from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .db import Base, str_255, str_300, str_400

if TYPE_CHECKING:
    from .photo_type import PhotoType


class Photo(Base):
    __tablename__ = 'photo'

    title: Mapped[str_255]
    img_url: Mapped[str_400]
    slug: Mapped[str_300]
    type_id: Mapped[int] = mapped_column(ForeignKey('photo_type.id'), nullable=False)
    photo_type: Mapped[list["PhotoType"]] = relationship(back_populates='photos')
    is_published: Mapped[bool] = mapped_column(default=True)
