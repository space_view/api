import enum

from sqlalchemy import UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from models.db import Base, list_str_400, str_100, str_255, str_400


class RawPhotoStatus(enum.Enum):
    NEW = 1
    APPROVED = 2
    DECLINED = 3


class RawPhoto(Base):
    __tablename__ = 'raw_photo'

    full_path: Mapped[str_400]
    category: Mapped[str_255 | None]
    title: Mapped[str_255]
    img_urls: Mapped[list_str_400]
    type: Mapped[str_100]
    status: Mapped[RawPhotoStatus] = mapped_column(default=RawPhotoStatus.NEW, nullable=False)

    __table_args__ = (UniqueConstraint('full_path', name='_full_path_unique'),)
