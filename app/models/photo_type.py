from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped, mapped_column, relationship

from .db import Base, str_255

if TYPE_CHECKING:
    from .photo import Photo


class PhotoType(Base):
    __tablename__ = 'photo_type'

    name: Mapped[str_255]
    slug: Mapped[str_255]
    photos: Mapped[list['Photo']] = relationship('Photo', back_populates='photo_type')
    is_published: Mapped[bool] = mapped_column(default=False)
