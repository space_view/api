from fastapi import Header, HTTPException
from starlette import status

from config import settings


def check_admin_user(x_admin_token: str = Header()) -> None:
    if settings.admin_access_key and x_admin_token != settings.admin_access_key:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
        )
