import pytest

from crud.crud_base import CRUDException
from crud.raw_photo_crud import RawPhotoCRUD

pytestmark = pytest.mark.asyncio


async def test_get_success(init_raw_photos: None, raw_photo_crud: RawPhotoCRUD):
    raw_photo = await raw_photo_crud.get_by_id(5)

    assert raw_photo.id == 5


async def test_row_not_exist(raw_photo_crud) -> None:
    with pytest.raises(CRUDException):
        await raw_photo_crud.get_by_id(1)
