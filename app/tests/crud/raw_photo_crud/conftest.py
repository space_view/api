import os
import shutil

import pytest
import pytest_asyncio

from config import settings
from crud.raw_photo_crud import RawPhotoCRUD
from models.raw_photo import RawPhoto, RawPhotoStatus


@pytest_asyncio.fixture
async def init_raw_photos(session):
    for item in range(1, 4):
        for status in RawPhotoStatus:
            path = f'https://example.com/{item}_{status.value}/'
            data = {
                'full_path': path,
                'category': None,
                'img_urls': [f'{path}image.png', f'{path}image.png'],
                'title': f'example title {item}',
                'type': 'perseverance_rover',
                'status': status,
            }
            session.add(RawPhoto(**data))
    await session.commit()


@pytest.fixture
def raw_photo_crud(session):
    yield RawPhotoCRUD(session=session)


@pytest.fixture(scope='module')
def image_url():
    return 'https://example.com/the_Moon.jpeg'


@pytest.fixture
def request_file_mock_setup(image_url):
    def _request_file_mock_setup(case, mocker):
        cases = {
            'success_response': {
                'status': 200,
                'body': b'This is the Moon',
                'content_type': 'document',
            },
            'not_found': {
                'status': 404,
                'body': 'Not Found',
                'content_type': 'text/plain',
            },
            'empty_body': {
                'status': 200,
                'body': b'',
                'content_type': 'document',
            },
        }

        case = cases.get(case)
        mocker.get(image_url, headers={'Content-Type': case['content_type']}, status=case['status'], body=case['body'])

    yield _request_file_mock_setup


@pytest.fixture(scope='module')
def test_image_folder():
    images_folder = 'images_test'
    old_image_folder = settings.image_folder
    settings.image_folder = images_folder

    try:
        os.mkdir(images_folder)
    except FileExistsError:
        ...

    yield

    settings.image_folder = old_image_folder
    shutil.rmtree(images_folder)
