import pytest

from crud.raw_photo_crud import RawPhotoCRUD, RawPhotoStatus

pytestmark = pytest.mark.asyncio


@pytest.mark.parametrize('status', [status for status in RawPhotoStatus])
async def test_filter_by_status(session, status, init_raw_photos, raw_photo_crud) -> None:
    raw_photos = await raw_photo_crud.get_photos_by_status(status=status)

    assert len(raw_photos) == 3
    for raw_photo in raw_photos:
        assert raw_photo.status == status


async def test_limit_one(session, init_raw_photos, raw_photo_crud) -> None:
    raw_photos = await raw_photo_crud.get_photos_by_status(limit=1)
    assert len(raw_photos) == 1
    assert raw_photos[0].id == 1

    raw_photos = await RawPhotoCRUD(session=session).get_photos_by_status(limit=1, status=RawPhotoStatus.DECLINED)
    assert len(raw_photos) == 1
    assert raw_photos[0].status == RawPhotoStatus.DECLINED


async def test_limit_multiple(session, init_raw_photos, raw_photo_crud) -> None:
    raw_photos = await raw_photo_crud.get_photos_by_status(limit=4)

    assert len(raw_photos) == 4
    assert raw_photos[0].id == 1
    assert raw_photos[1].id == 2
    assert raw_photos[2].id == 3
    assert raw_photos[3].id == 4
