from unittest.mock import AsyncMock, patch

import pytest
from aioresponses import aioresponses
from sqlalchemy.ext.asyncio import AsyncSession

from crud.crud_base import CRUDException
from crud.raw_photo_crud import RawPhotoCRUD
from file_loader import LoadFileError, load_photo
from models.raw_photo import RawPhotoStatus

pytestmark = pytest.mark.asyncio


@patch('crud.raw_photo_crud.load_photo')
async def test_from_new_to_declined(
    load_photo_mock: AsyncMock, session: AsyncSession, init_raw_photos: None, raw_photo_crud: RawPhotoCRUD
) -> None:
    new_raw_photos = await raw_photo_crud.get_photos_by_status(
        status=RawPhotoStatus.NEW,
        limit=1,
    )
    new_raw_photo = new_raw_photos[0]
    assert new_raw_photo.status == RawPhotoStatus.NEW

    await raw_photo_crud.change_status_photo(new_raw_photo.id, RawPhotoStatus.DECLINED)
    await session.refresh(new_raw_photo)

    assert new_raw_photo.status == RawPhotoStatus.DECLINED
    load_photo_mock.assert_not_called()


@patch('file_loader.load_photo')
@pytest.mark.parametrize('status_from', [RawPhotoStatus.DECLINED, RawPhotoStatus.APPROVED])
async def test_change_status_to_new_not_allowed(
    load_photo_mock: AsyncMock,
    session: AsyncSession,
    init_raw_photos: None,
    raw_photo_crud: RawPhotoCRUD,
    status_from: RawPhotoStatus,
) -> None:
    new_raw_photos = await raw_photo_crud.get_photos_by_status(
        status=status_from,
        limit=1,
    )
    raw_photo = new_raw_photos[0]

    with pytest.raises(CRUDException):
        await raw_photo_crud.change_status_photo(raw_photo.id, RawPhotoStatus.NEW)

    await session.refresh(raw_photo)

    assert raw_photo.status == status_from
    load_photo_mock.assert_not_called()


@pytest.mark.parametrize('case', ['success_response', 'not_found', 'empty_body'])
async def test_success_load_photo(test_image_folder, image_url, request_file_mock_setup, case):
    with aioresponses() as m:
        request_file_mock_setup(case, m)
        if case == 'success_response':
            with patch('file_loader.uuid.uuid4', return_value='1234'):
                result = await load_photo(image_url)
                assert result == 'images_test/1234.jpeg'
        else:
            with pytest.raises(LoadFileError):
                await load_photo(image_url)
