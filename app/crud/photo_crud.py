from typing import Any, Dict

from slugify import slugify

from crud.crud_base import CRUDBase
from models.photo import Photo
from models.photo_type import PhotoType


class PhotoCRUD(CRUDBase):
    async def create(self, data: dict) -> Photo:
        db_obj = Photo(**data)
        self.session.add(db_obj)
        await self.session.commit()
        return db_obj

    async def create_from_raw_photo(
        self, image_path: str, title: str, photo_type: PhotoType, photo_number: int = 0
    ) -> Photo:
        data: Dict[str, Any] = {}
        if photo_number > 0:
            title += f' {photo_number}'

        data['title'] = title
        data['slug'] = slugify(title)
        data['img_url'] = image_path
        data['is_published'] = True
        data['type_id'] = photo_type.id
        photo = await self.create(data)
        return photo
