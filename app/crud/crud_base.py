from sqlalchemy.ext.asyncio import AsyncSession


class CRUDException(Exception):
    pass


class CRUDBase:
    """
    Базовый класс для определения CRUD операций.
    :ivar model: Модель описывающая таблицу, для которой производятся операции.
    """

    session: AsyncSession

    def __init__(self, session: AsyncSession):
        """
        :param session: Объект сессии
        """
        self.session = session
