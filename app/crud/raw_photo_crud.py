from typing import Sequence

from sqlalchemy import select
from sqlalchemy.exc import NoResultFound

from crud.crud_base import CRUDBase, CRUDException
from crud.photo_crud import PhotoCRUD
from crud.photo_type_crud import PhotoTypeCRUD
from file_loader import LoadFileError, load_photo
from models.raw_photo import RawPhoto, RawPhotoStatus


class RawPhotoCRUD(CRUDBase):
    async def create(self, data: dict) -> RawPhoto:
        db_obj = RawPhoto(**data)
        self.session.add(db_obj)
        await self.session.commit()
        return db_obj

    async def get_all(self) -> Sequence[RawPhoto]:
        return (await self.session.scalars(select(RawPhoto))).all()

    async def get_by_id(self, pk: int) -> RawPhoto:
        try:
            return (await self.session.scalars(select(RawPhoto).where(RawPhoto.id == pk))).one()
        except NoResultFound:
            raise CRUDException('Объект фото не найден')

    async def get_photos_by_status(
        self,
        limit: int = 20,
        status: RawPhotoStatus | None = None,
    ) -> Sequence[RawPhoto]:
        query = select(RawPhoto)

        if status:
            query = query.where(RawPhoto.status == status)

        return (await self.session.scalars(query.order_by(RawPhoto.id).limit(limit))).all()

    async def approve_raw_photo(self, raw_photo: RawPhoto):
        photo_type = await PhotoTypeCRUD(session=self.session).get_or_create(raw_photo.type)
        photo_crud = PhotoCRUD(session=self.session)
        urls_counter = 0
        for url in raw_photo.img_urls:
            try:
                local_path = await load_photo(url)
            except LoadFileError:
                continue
            else:
                await photo_crud.create_from_raw_photo(
                    title=raw_photo.title, image_path=local_path, photo_type=photo_type, photo_number=urls_counter
                )
                urls_counter += 1

        raw_photo.status = RawPhotoStatus.APPROVED
        await self.session.commit()

    async def change_status_photo(self, photo_id: int, new_status: RawPhotoStatus) -> None:
        raw_photo = (await self.session.scalars(select(RawPhoto).where(RawPhoto.id == photo_id))).first()
        # Разрешается переводить только из статуса NEW
        if (
            raw_photo
            and raw_photo.status == RawPhotoStatus.NEW
            and new_status in [RawPhotoStatus.DECLINED, RawPhotoStatus.APPROVED]
        ):
            if new_status == RawPhotoStatus.APPROVED:
                await self.approve_raw_photo(raw_photo)
            else:
                raw_photo.status = new_status
                await self.session.commit()
        else:
            raise CRUDException('невозможно изменить статус')
