from sqlalchemy import select
from sqlalchemy.orm.exc import NoResultFound

from crud.crud_base import CRUDBase, CRUDException
from models.photo_type import PhotoType


class PhotoTypeCRUD(CRUDBase):

    TYPES_AND_SLUG = {
        'perseverance_rover': 'perseverance',
    }

    SLUG_AND_NAMES = {'perseverance': 'Ровер Персеверанс'}

    async def get_or_create(self, parser_type: str) -> PhotoType:
        slug = self.TYPES_AND_SLUG.get(parser_type)
        if not slug:
            raise CRUDException(f'Не найден slug для типа {parser_type}')

        try:
            photo_type = (await self.session.scalars(select(PhotoType).where(PhotoType.slug == slug))).one()
        except NoResultFound:
            photo_type = PhotoType(name=self.SLUG_AND_NAMES.get(slug), slug=slug)
            self.session.add(photo_type)
            await self.session.commit()
        return photo_type
