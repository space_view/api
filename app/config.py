from pydantic import BaseSettings


class Settings(BaseSettings):
    postgres_url: str

    rabbitmq_login: str
    rabbitmq_password: str
    rabbitmq_host: str
    rabbitmq_port: str = '5672'
    rabbitmq_vhost: str = '/'
    rabbitmq_queue: str

    admin_access_key: str | None
    image_folder = str = 'images'


settings = Settings()
