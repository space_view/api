"""initial

Revision ID: 8eff4f036d04
Revises: 
Create Date: 2023-02-08 15:33:56.451003

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8eff4f036d04'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        'photo_type',
        sa.Column('name', sa.String(length=255), nullable=False),
        sa.Column('slug', sa.String(length=255), nullable=False),
        sa.Column('is_published', sa.Boolean(), nullable=False),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
    )
    op.create_table(
        'raw_photo',
        sa.Column('full_path', sa.String(length=400), nullable=False),
        sa.Column('category', sa.String(length=255), nullable=True),
        sa.Column('title', sa.String(length=255), nullable=False),
        sa.Column('img_urls', sa.ARRAY(sa.String(length=400)), nullable=False),
        sa.Column('type', sa.String(length=100), nullable=False),
        sa.Column('status', sa.Enum('NEW', 'APPROVED', 'DECLINED', name='rawphotostatus'), nullable=False),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('full_path', name='_full_path_unique'),
    )
    op.create_table(
        'photo',
        sa.Column('title', sa.String(length=255), nullable=False),
        sa.Column('img_url', sa.String(length=400), nullable=False),
        sa.Column('slug', sa.String(length=300), nullable=False),
        sa.Column('type_id', sa.Integer(), nullable=False),
        sa.Column('is_published', sa.Boolean(), nullable=False),
        sa.Column('id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ['type_id'],
            ['photo_type.id'],
        ),
        sa.PrimaryKeyConstraint('id'),
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('photo')
    op.drop_table('raw_photo')
    op.drop_table('photo_type')
    op.execute("""DROP TYPE rawphotostatus""")
    # ### end Alembic commands ###
