from fastapi import APIRouter, FastAPI

from admin.router import admin_router

app = FastAPI(title='Space View Backend')

api_router = APIRouter()
api_router.include_router(admin_router, tags=['admin'])


app.include_router(api_router)
