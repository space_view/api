import os
import uuid

import aiofiles
import aiohttp

from config import settings


class LoadFileError(Exception):
    ...


def build_path(file_url: str) -> str:
    file_url_split = file_url.split('/')
    file_name = file_url_split[-1]
    ext = file_name.split('.')[-1]
    hashed_file_name = f'{uuid.uuid4()}.{ext}'
    return f'{settings.image_folder}/{hashed_file_name}'


async def load_photo(img_url: str) -> str:
    try:
        async with aiohttp.request(url=img_url, method='get') as resp:
            if resp.status == 200:
                content = await resp.read()
                if len(content) > 0:
                    path = build_path(img_url)
                    f = await aiofiles.open(os.path.abspath(path), mode='wb')
                    await f.write(content)
                    await f.close()
                    return path
            raise LoadFileError(f'Не удалось сохранить файл, код ответа - {resp.status}')
    except aiohttp.ClientError:
        raise LoadFileError('Не удалось сохранить файл')
