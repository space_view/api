# Бэкенд API для фото

### Запуск сервиса в режиме разработки

#### Перед первым запуском
❗️Скопируйте `.env.example` в `.env` и используйте новый файл для переопределения переменных среды.  

Для запуска всех сервисов:
```shell
docker compose up
```

### Запуск линтеров и юнит тестов

### Линтеры / форматеры кода
Используется `flake8`, `mypy`, `isort` и `black` на проекте.

Для проверки:
```shell
docker compose run --rm api make lint
```
Для форматирования:
```shell
docker compose run --rm api make format
```

### Миграции БД
Для миграций используется alembic.  
После изменения схемы БД можно сгенерировать миграцию.  
Автогенерация миграций:
```shell
# зайти в контейнер
docker compose run --rm api /bin/bash

alembic revision --autogenerate -m "Название миграции (напр. Добавлено поле Название)"
alembic upgrade head
```
